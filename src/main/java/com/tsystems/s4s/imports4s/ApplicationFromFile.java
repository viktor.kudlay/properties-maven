package com.tsystems.s4s.imports4s;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

public class ApplicationFromFile {
    public static void main(String[] args) {

        Properties appProps = new Properties();


        try {
            appProps.load(new FileInputStream(args[0]));
        } catch (IOException e) {
            e.printStackTrace();
        }


        System.out.println("server = " + appProps.getProperty("server"));
        System.out.println("port = " + appProps.getProperty("port"));
        System.out.println("login = " + appProps.getProperty("login"));
        System.out.println("pass = " + appProps.getProperty("pass"));
    }
}